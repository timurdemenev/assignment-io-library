section .text


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
	cmp byte [rdi + rax], 0
    jz  .end
    inc rax
    jmp .loop
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    lea rsi, [rsp]
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0ah
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r9, 10
	mov rax, rdi
	mov rdi, rsp
	dec rdi
	push 0
	sub rsp, 16
.loop:
	xor rdx, rdx
	div r9
	add rdx, '0'
	dec rdi
	mov [rdi], dl
	cmp rax, 0
	je .end
	jmp .loop
.end:
	call print_string
	add rsp, 24
	ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .printing_int
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.printing_int:
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rax, rax
.checking:
	mov cl, byte[rdi + rax]
	cmp cl, byte[rsi + rax]
	jne .not_eq
	cmp cl, 0
	je .eq
	inc rax
	jmp .checking
.eq:
	mov rax, 1
	jmp .end
.not_eq:
	xor rax, rax
.end:
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rdx, rdx
.read_spaces_loop:
	push rdi
	push rsi
	push rdx
	call read_char
	pop rdx
	pop rsi
	pop rdi
	cmp rax, 0x20
	je .read_spaces_loop
	cmp rax, 0x9
	je .read_spaces_loop
	cmp rax, 0xA
	je .read_spaces_loop
.read_word_loop:
	cmp rax, 0
	je .complete_word
	mov byte[rdi + rdx], al
	inc rdx
	cmp rdx, rsi
	jge .fail_read
	push rdi
	push rsi
	push rdx
	call read_char
	pop rdx
	pop rsi
	pop rdi
	cmp rax, 0x20
	je .complete_word
	cmp rax, 0x9
	je .complete_word
	cmp rax, 0xA
	je .complete_word
	jmp .read_word_loop
.complete_word:
	mov byte[rdi + rdx], 0
	mov rax, rdi
	jmp .end_read_word
.fail_read:
	xor rax, rax
	xor rdx, rdx
.end_read_word:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
.parse_uint_loop:
	mov r8b, byte[rdi + rdx]
	cmp r8, '0'
	jl .end_parse_uint
	cmp r8, '9'
	jg .end_parse_uint
	cmp r8, 0
	je .end_parse_uint
	imul rax, 10
	sub r8, '0'
	add rax, r8
	inc rdx
	jmp .parse_uint_loop
.end_parse_uint:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rsi, rsi
	cmp byte[rdi], '-'
	jne .parse_uint
	mov rsi, 1
	inc rdi
.parse_uint:
	call parse_uint
	cmp rdx, 0
	je .end_parse_int
	cmp rsi, 0
	je .end_parse_int
	inc rdx
	neg rax
.end_parse_int:
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	xor r8, r8
.copy_loop:
	cmp rax, rdx
	jge .copy_fail
	mov r8b, byte[rdi + rax]
	mov byte[rsi + rax], r8b
	cmp r8b, 0
	je .copy_end
	inc rax
	jmp .copy_loop
.copy_fail:
	xor rax, rax
.copy_end:
	ret
